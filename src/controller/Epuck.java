package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import jssc.SerialPortException;

public class Epuck {

	private String portName;
	private String id;
	private String vulnerability;
	private ArrayList<Epuck> neighbors;
	private String k;
	private double positionX;
	private double positionY;
	private BluetoothHandler bluetooth;
	private Message message;
	private Fiducial fiducial;
	boolean connected;

	public Epuck(String portName) {
		super();
		this.portName = portName;
		neighbors = new ArrayList<Epuck>();
		this.message = new Message();
		Random rand = new Random();
		this.k = String.valueOf(rand.nextInt(9) + 1);

		if (portName.startsWith("remote")) {
			String[] parts = portName.split("-");
			this.id = parts[1];
		} else {
			bluetooth = new BluetoothHandler(portName);

		}
	}

	public static void main(String[] args) throws InterruptedException, IOException, SerialPortException {

		Epuck epuck1 = new Epuck("/dev/tty.e-puck_2926-COM1");

		byte[] request = Message.dinamicMessage("I");
		String req = new String(request);
		System.out.println("ENVIOU" + req);
		epuck1.getBluetooth().sendBytes(request);

		// String received0= epuck1.getBluetooth().readString();
		String received0 = epuck1.waitString(1);
		System.out.println("RECEBEU: " + received0);

		// byte[] test =
		// Message.dinamicMessage("M,10.721,11.335,120.9858888,25.35,26.15");

		for (int i = 0; i < 10; i++) {

			byte[] data = Message.dinamicMessage("V,0.157,0.0,0.0,90.0");

			String str = new String(data);
			System.out.println("ENVIOU" + str);
			epuck1.getBluetooth().sendBytes(data);
			Thread.sleep(300);

			byte[] n1 = Message.dinamicMessage("V,9000,2979,7.0,7.0,0.98");
			String str7 = new String(n1);
			System.out.println("ENVIOU" + str7);
			epuck1.getBluetooth().sendBytes(n1);
			Thread.sleep(300);

			byte[] n2 = Message.dinamicMessage("V,4000,2979,140.98,140.98,140.98");
			String str2 = new String(n2);
			System.out.println("ENVIOU" + str2);
			epuck1.getBluetooth().sendBytes(n2);
			Thread.sleep(300);

			byte[] n3 = Message.dinamicMessage("V,6000,2979,140.98,140.98,140.98");
			String str17 = new String(n3);
			System.out.println("ENVIOU" + str17);
			epuck1.getBluetooth().sendBytes(n3);
			Thread.sleep(300);

			byte[] n4 = Message.dinamicMessage("V,7000,2979,140.98,140.98,140.98");
			String str21 = new String(n4);
			System.out.println("ENVIOU" + str21);
			epuck1.getBluetooth().sendBytes(n4);
			Thread.sleep(300);

			byte[] finish1 = Message.dinamicMessage("f");
			String str3 = new String(finish1);
			System.out.println("ENVIOU" + str3);
			epuck1.getBluetooth().sendBytes(finish1);
			Thread.sleep(300);

			byte[] h1 = Message.dinamicMessage("V,2000,3000,10.0,10.0,0.98");
			String str4 = new String(h1);
			System.out.println("ENVIOU" + str4);
			epuck1.getBluetooth().sendBytes(h1);
			Thread.sleep(300);

			byte[] h2 = Message.dinamicMessage("V,3000,3168,15.0,10.0,0.98");
			String str5 = new String(h2);
			System.out.println("ENVIOU" + str5);
			epuck1.getBluetooth().sendBytes(h2);
			Thread.sleep(300);

			byte[] h3 = Message.dinamicMessage("V,5000,3000,15.0,15.0,0.98");
			String str6 = new String(h3);
			System.out.println("ENVIOU" + str6);
			epuck1.getBluetooth().sendBytes(h3);
			Thread.sleep(300);

			byte[] h4 = Message.dinamicMessage("V,7000,3168,20.0,15.0,0.98");
			String str10 = new String(h4);
			System.out.println("ENVIOU" + str10);
			epuck1.getBluetooth().sendBytes(h4);
			Thread.sleep(300);

			byte[] h5 = Message.dinamicMessage("V,3000,3168,10.98,10.98,10.98");
			String str12 = new String(h5);
			System.out.println("ENVIOU" + str12);
			epuck1.getBluetooth().sendBytes(h5);
			Thread.sleep(300);

			byte[] finish2 = Message.dinamicMessage("f");
			String str11 = new String(finish2);
			System.out.println("ENVIOU" + str11);
			epuck1.getBluetooth().sendBytes(finish2);
			Thread.sleep(300);

			String received = epuck1.waitString(13);
			System.out.println("RECEBEU: " + received);

			String received2 = epuck1.waitString(4);
			System.out.println("RECEBEU: " + received2);
		}
	}

	public void sendProximityRange(final String range) {
		new Thread() {
			@Override
			public void run() {

				byte[] msg = message.SEND_PROX;

				byte[] byteRange = range.getBytes();
				msg[5] = byteRange[0];
				msg[6] = byteRange[1];
				msg[7] = byteRange[2];

				bluetooth.sendBytes(msg);
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
				System.out.println("mandou o range:" + msg.toString());
			}
		}.start();
	}

	public void sendMove(final String value, boolean flag) {
		new Thread() {
			@Override
			public void run() {

				byte[] msg = message.SEND_ROTATE;

				byte[] byteValue = value.getBytes();
				msg[5] = byteValue[0];
				msg[6] = byteValue[1];
				msg[7] = byteValue[2];

				bluetooth.sendBytes(msg);
				try {
					Thread.sleep(8000);

					if (flag) {

						byte[] bytesReceived = bluetooth.readBytes();
						String str = new String(bytesReceived);
						System.out.println(str);
					}

				} catch (InterruptedException e) {

					e.printStackTrace();
				}
				System.out.println("mandou o angulo:" + msg.toString());
			}
		}.start();
	}

	public void sendFloatMove(final String value, boolean flag) {
		new Thread() {
			@Override
			public void run() {

				byte[] test = Message.dinamicMessage(value);

				String str = new String(test);
				System.out.println("ENVIOU" + str);

				bluetooth.sendBytes(test);
				try {
					Thread.sleep(300);
					String received = bluetooth.readString();
					System.out.println("RECEBEU: " + received);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SerialPortException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.start();
	}

	public String getVulnerability() throws InterruptedException, SerialPortException {

		if (portName.startsWith("remote")) {

		} else {

			String msgVulnerability = "V,0.0," + (float) getPositionX() + "," + (float) getPositionY() + ","
					+ getFiducial().getAngle();
			byte[] data = Message.dinamicMessage(msgVulnerability);
			String str = new String(data);
			System.out.println("ENVIOU" + str);
			bluetooth.sendBytes(data);
			Thread.sleep(300);

			for (int i = 0; i < getNeighbors().size(); i++) {

				String msgNeighbor = "V," + getNeighbors().get(i).getId() + "," + getId() + ","
						+ getNeighbors().get(i).getPositionX() + "," + getNeighbors().get(i).getPositionY() + ",0.0";
				byte[] n = Message.dinamicMessage(msgNeighbor);
				String strneighbor = new String(n);
				System.out.println("ENVIOU" + strneighbor);
				bluetooth.sendBytes(n);
				Thread.sleep(300);
			}

			byte[] finishNeighbors = Message.dinamicMessage("f");
			String strFinish = new String(finishNeighbors);
			System.out.println("ENVIOU" + strFinish);
			bluetooth.sendBytes(finishNeighbors);
			Thread.sleep(300);

			for (int i = 0; i < getNeighbors().size(); i++) {

				for (int j = 0; j < getNeighbors().get(i).getNeighbors().size(); j++) {

					String msgTwoHop = "V," + getNeighbors().get(i).getNeighbors().get(j).getId() + ","
							+ getNeighbors().get(i).getId() + ","
							+ getNeighbors().get(i).getNeighbors().get(j).getPositionX() + ","
							+ getNeighbors().get(i).getNeighbors().get(j).getPositionY() + ",0.0";

					boolean flag = false;
					for (int k = 0; k < getNeighbors().size(); k++) {
						if (getNeighbors().get(k).getId().equals(getNeighbors().get(i).getNeighbors().get(j).getId())) {
							flag = true;
						}
						if (getId().equals(getNeighbors().get(i).getNeighbors().get(j).getId())) {
							flag = true;
						}
					}
					if (!flag) {

						byte[] n = Message.dinamicMessage(msgTwoHop);
						String strTwoHop = new String(n);
						System.out.println("ENVIOU" + strTwoHop);
						bluetooth.sendBytes(n);
						Thread.sleep(300);
					}
				}
			}

			byte[] finishTwoHops = Message.dinamicMessage("f");
			String strFinishTwoHop = new String(finishTwoHops);
			System.out.println("ENVIOU" + strFinishTwoHop);
			bluetooth.sendBytes(finishTwoHops);
			Thread.sleep(300);

			String received = waitString(2);
			System.out.println("RECEBEU: " + received);
			Thread.sleep(300);

			if (received.contains("S")) {
				return "S";

			} else {

				return "N";
			}

		}
		return vulnerability;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public String getId() throws InterruptedException {

		if (this.id != null) {
			// System.out.println("pegou o id:" + id);
			return id;
		} else {

			if (isConnected()) {

				byte[] msg = message.dinamicMessage("I");

				String str = new String(msg);
				System.out.println("ENVIOU" + str);
				bluetooth.sendBytes(msg);

				Thread.sleep(150);

				String received;
				try {
					received = waitString(1);

					System.out.println("RECEBEU: " + received);
					id = received.substring(5, 9);

					System.out.println("pegou o id:" + id);

				} catch (SerialPortException e) {

					e.printStackTrace();
				}

				return id;
			}

			return null;
		}
	}

	public void sendStart() throws InterruptedException, IOException {

		if (portName.startsWith("remote")) {

			String msg = id + "-02-00";
			String response = Client.request(msg);
			System.out.println(response);
		} else {
			// bluetooth.connect();
			byte[] msg = message.SEND_START;
			bluetooth.sendBytes(msg);
			// bluetooth.disconnect();
			System.out.println("enviou o START");
		}
	}

	public void sendStop() throws InterruptedException, IOException {

		if (portName.startsWith("remote")) {

			String msg = id + "-03-00";
			String response = Client.request(msg);
			System.out.println(response);
		} else {
			// bluetooth.connect();
			byte[] msg = message.SEND_STOP;
			bluetooth.sendBytes(msg);
			// bluetooth.disconnect();
			System.out.println("enviou o STOP");
		}
	}

	public boolean connect() throws InterruptedException {

		if (portName.startsWith("remote")) {

			return true;
		} else {
			if (bluetooth.isFlagConnected()) {
				bluetooth.disconnect();
				Thread.sleep(1000);
				connected = bluetooth.connect();
				return connected;

			} else {
				connected = bluetooth.connect();
				return connected;
			}
		}
	}

	public void disconnect() {
		if (portName.startsWith("remote")) {

		} else {
			bluetooth.disconnect();
			connected = false;
		}
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<Epuck> getNeighbors() {
		return neighbors;
	}

	public void setNeighbors(ArrayList<Epuck> neighbors) {
		this.neighbors = neighbors;
	}

	public void addNeighbor(Epuck neighbor) {
		this.neighbors.add(neighbor);
	}

	public void removeNeighbor(Epuck neighbor) {

		for (int i = 0; i < neighbors.size(); i++) {
			if (neighbors.get(i).getPortName().equals(neighbor.getPortName())) {
				this.neighbors.remove(i);
			}
		}
	}

	public String getK() throws InterruptedException, IOException {

		if (portName.startsWith("remote")) {

			String msg = id + "-04-";

			String quantityNeighbors = convertValueToFormatedString(getNeighbors().size());
			msg = msg.concat(quantityNeighbors + "&");

			for (int i = 0; i < getNeighbors().size(); i++) {
				String NeighborIDValue = convertValueToFormatedStringID(Integer.parseInt(getNeighbors().get(i).id));
				msg = msg.concat(NeighborIDValue + ",");
				String NeighborKValue = convertValueToFormatedString(Integer.parseInt(getNeighbors().get(i).k));
				msg = msg.concat(NeighborKValue + ",");

			}
			String kReceived = Client.request(msg);
			System.out.println(kReceived);
			k = kReceived;

			if (this.fiducial != null) {
				this.fiducial.setKhapa(Integer.parseInt(k));
				System.out.println("está entrando no setKhapa");
			}
			return k;
		} else {
			// bluetooth.connect();

			byte[] msg1 = message.REQUEST_K;

			String quantityNeighbors = convertValueToFormatedString(getNeighbors().size());
			byte[] qNeighbors = quantityNeighbors.getBytes();
			msg1[5] = qNeighbors[0];
			msg1[6] = qNeighbors[1];
			msg1[7] = qNeighbors[2];

			bluetooth.sendBytes(msg1);

			for (int i = 0; i < getNeighbors().size(); i++) {
				byte[] msg2 = message.SEND_NEIGHBORS;

				String NeighborIDValue = convertValueToFormatedStringID(Integer.parseInt(getNeighbors().get(i).id));
				byte[] NeighborID = NeighborIDValue.getBytes();
				msg2[4] = NeighborID[0];
				msg2[5] = NeighborID[1];
				msg2[6] = NeighborID[2];
				msg2[7] = NeighborID[3];

				bluetooth.sendBytes(msg2);

				byte[] msg3 = message.SEND_NEIGHBORS;

				String NeighborKValue = convertValueToFormatedString(Integer.parseInt(getNeighbors().get(i).k));
				byte[] NeighborK = NeighborKValue.getBytes();
				msg3[5] = NeighborK[0];
				msg3[6] = NeighborK[1];
				msg3[7] = NeighborK[2];

				bluetooth.sendBytes(msg3);
			}
			byte[] kReceived = bluetooth.readBytes(6);
			String str = new String(kReceived).substring(3, 6);
			k = str;

			// bluetooth.disconnect();

			if (this.fiducial != null) {

				try {
					this.fiducial.setKhapa(Integer.parseInt(k));
				} catch (NumberFormatException e) {
					System.out.println("formou um numero errado, provavelmente string vazia");
				}
			}

			System.out.println(k);
			return k;
		}
	}

	public void setK(String k) {
		this.k = k;
	}

	public double getPositionX() {
		return positionX;
	}

	public void setPositionX(double positionX) {
		this.positionX = positionX;
	}

	public double getPositionY() {
		return positionY;
	}

	public void setPositionY(double positionY) {
		this.positionY = positionY;
	}

	public void setPositionXY(double x, double y) {
		this.positionX = x;
		this.positionY = y;
	}

	public void addNeighbors(Epuck epuck) {
		this.neighbors.add(epuck);
	}

	public void removeNeighbors(Epuck epuck) throws InterruptedException {
		for (int i = 0; i < this.neighbors.size(); i++) {
			if (this.neighbors.get(i).getId().equals(epuck.getId())) {
				this.neighbors.remove(i);
			}
		}
	}

	public static String convertValueToFormatedString(int value) {

		StringBuilder resultValue = new StringBuilder("000");
		String converted = String.valueOf(value);

		for (int i = 1; i <= converted.length(); i++) {
			resultValue.setCharAt(resultValue.length() - i, converted.charAt(converted.length() - i));
		}
		return resultValue.toString();
	}

	public static String convertValueToFormatedStringID(int value) {

		StringBuilder resultValue = new StringBuilder("0000");
		String converted = String.valueOf(value);

		for (int i = 1; i <= converted.length(); i++) {
			resultValue.setCharAt(resultValue.length() - i, converted.charAt(converted.length() - i));
		}
		return resultValue.toString();
	}

	public Fiducial getFiducial() {
		return fiducial;
	}

	public void setFiducial(Fiducial fiducial) {
		this.fiducial = fiducial;
	}

	public BluetoothHandler getBluetooth() {
		return bluetooth;
	}

	public boolean isConnected() {
		return connected;
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}

	public String getResponseVulnerability() {
		return null;
	}

	public String waitString(int length) throws InterruptedException, SerialPortException {

		String result = "";
		int overflow = 0;
		Thread.sleep(100);

		while (result.equals("") && (overflow <= 10)) {

			if (bluetooth.getSerialPort().getInputBufferBytesCount() >= length) {
				result = bluetooth.readString();
			}
			Thread.sleep(50);
			overflow++;
		}

		if (result.equals("")) {
			return "vazio";
		}

		return result;
	}

	public String getVulnerability(String param) {
		try {
			String parts[] = param.split("&");

			for (int i = 0; i < parts.length; i++) {
				System.out.println(parts[i]);
			}

			byte[] data = Message.dinamicMessage(parts[0]);
			String str = new String(data);
			System.out.println("ENVIOU" + str);
			bluetooth.sendBytes(data);

			Thread.sleep(100);

			int i = 1;

			while (!parts[i].equals("f")) {

				byte[] datai = Message.dinamicMessage(parts[i]);
				String stri = new String(datai);
				System.out.println("ENVIOU" + stri);
				bluetooth.sendBytes(datai);
				i++;
				Thread.sleep(100);
			}

			byte[] finishNeighbors = Message.dinamicMessage("f");
			String strf = new String(finishNeighbors);
			System.out.println("ENVIOU" + strf);
			bluetooth.sendBytes(finishNeighbors);
			i++;
			Thread.sleep(100);

			while (!parts[i].equals("f")) {

				byte[] datai = Message.dinamicMessage(parts[i]);
				String stri = new String(datai);
				System.out.println("ENVIOU" + stri);
				bluetooth.sendBytes(datai);
				i++;
				Thread.sleep(100);
			}

			byte[] finishTwo = Message.dinamicMessage("f");
			String strt = new String(finishTwo);
			System.out.println("ENVIOU" + strt);
			bluetooth.sendBytes(finishTwo);

			Thread.sleep(300);

			String received = waitString(2);
			System.out.println("RECEBEU: " + received);
			Thread.sleep(300);

			if (received.contains("S")) {
				return "S";

			} else {

				return "N";
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SerialPortException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ArrayIndexOutOfBoundsException e2) {
			System.out.println("falhou em pegar a vulnerabilidade");
			return "0.000";
		}

		vulnerability = "achounadacli";

		return vulnerability;

	}
}