package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.SelectableChannel;
import java.util.ArrayList;
import java.util.List;

class Server {

	private List<Epuck> epuckList;
	private int port;
	ServerSocket srvr;
	Socket skt;
	BufferedReader in;
	PrintWriter out;
	private boolean connected;

	public Server(List<Epuck> epuckList, int port) throws IOException {
		super();
		this.epuckList = epuckList;
		this.port = port;
		connected = false;
	}

	public void listen() throws InterruptedException, IOException {

		while (true) {
			srvr = new ServerSocket(1234);
			skt = srvr.accept();
			System.out.print("Server has connected!\n");
			out = new PrintWriter(skt.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
			connected = true;
			String response;

			while (connected) {
				try {
					while (!in.ready()) {
					}
					String line = in.readLine();
					System.out.println(line);
					response = processRequest(line);
					System.out.println("Respondeu " + response);
					out.println(response);

					if (response.equals("disconnect")) {
						srvr.close();
						skt.close();
					}

				} catch (IOException e) {
					System.out.print("Whoops! It didn't work!\n");
				}
			}
		}
	}

	private String processRequest(String line) throws InterruptedException, IOException {

		if (line.startsWith("teste")) {
			return "Servidor respondeu - teste ok";
		}

		if (line.startsWith("disconnect")) {
			connected = false;
			return "disconnect";
		}

		String[] parts = line.split("-");
		String epuckId = parts[0];
		String opCode = parts[1];
		String param = parts[2];

		for (int i = 0; i < epuckList.size(); i++) {

			if (epuckList.get(i).getId().equals(epuckId)) {

				switch (opCode) {
				case "01":
					return epuckList.get(i).getId();

				case "02":
					epuckList.get(i).sendStart();
					return "recebeu start";

				case "03":
					epuckList.get(i).sendStop();
					return "recebeu stop";

				case "04":
					return epuckList.get(i).getVulnerability(param);

				default:
					break;
				}
			}
		}
		return null;
	}

	public static void main(String args[]) throws IOException, InterruptedException {

		List<Epuck> list = new ArrayList<Epuck>();

		list.add(new Epuck("/dev/tty.e-puck_2816-COM1"));
		list.add(new Epuck("/dev/tty.e-puck_3054-COM1"));
		list.add(new Epuck("/dev/tty.e-puck_3177-COM1"));
		list.add(new Epuck("/dev/tty.e-puck_3118-COM1"));
		list.add(new Epuck("/dev/tty.e-puck_2926-COM1"));
		list.add(new Epuck("/dev/tty.e-puck_2979-COM1"));

		ConnectWindow connectWindow = new ConnectWindow(list);
		connectWindow.setVisible(true);
		connectWindow.setSize(640, 480);

		Server server = new Server(list, 1234);
		server.listen();
	}
}
