package controller;

import georegression.struct.point.Point2D_I32;
import georegression.struct.shapes.Quadrilateral_F64;
import jssc.SerialPortException;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Dimension;
import java.awt.Robot;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import view.CamWindow;
import view.DrawFiducial;
import view.RobotWindow;

import boofcv.alg.filter.binary.GThresholdImageOps;

public class Topology {

	private List<Epuck> epuckList;
	private Map<Integer, String> mapFiducial;
	private Dimension[] resolution;

	public Topology(Dimension[] resolution) {
		super();
		epuckList = new ArrayList<>();
		mapFiducial = new HashMap<Integer, String>();
		this.resolution = resolution;

	}

	public Topology(List<Epuck> epuckList) {
		super();
		this.epuckList = epuckList;
	}

	public List<Epuck> getEpuckList() {
		return epuckList;
	}

	public void setEpuckList(List<Epuck> epuckList) {
		this.epuckList = epuckList;
	}

	public void addEpuck(Epuck epuck) {
		this.epuckList.add(epuck);
	}

	public void removeEpuck(Epuck epuck) {
		this.epuckList.remove(epuck);
	}

	public Epuck getEpuckById(String id) throws InterruptedException {

		for (int i = 0; i < this.epuckList.size(); i++) {

			if (getEpuckList().get(i).getId() != null) {
				if (getEpuckList().get(i).getId().equals(id)) {
					return this.epuckList.get(i);
				}
			}

		}
		return null;
	}

	public void updateKHOPCA(ArrayList<Fiducial> fiducialList, int radiationRadius)
			throws InterruptedException, IOException {

		for (int i = 0; i < getEpuckList().size(); i++) {

			if (getEpuckList().get(i).getFiducial().isActive()) {

				for (int j = 0; j < getEpuckList().size(); j++) {

					if (getEpuckList().get(j).getFiducial().isActive()) {

						Point2D_I32 first = getEpuckList().get(i).getFiducial().getCenter();
						Point2D_I32 second = getEpuckList().get(j).getFiducial().getCenter();

						double distance = DrawFiducial.getEuclidianDistance(first, second);

						System.out.println("Distance" + distance);

						if ((distance <= radiationRadius) && (distance >= 5.0)) {

							if (!getEpuckList().get(i).getNeighbors().contains(getEpuckList().get(j))) {

								getEpuckList().get(i).addNeighbor(getEpuckList().get(j));
							}
						} else {

							if (getEpuckList().get(i).getNeighbors().contains(getEpuckList().get(j))) {

								getEpuckList().get(i).removeNeighbor(getEpuckList().get(j));
							}
						}
					} else {

						if (getEpuckList().get(i).getNeighbors().contains(getEpuckList().get(j))) {

							getEpuckList().get(i).removeNeighbor(getEpuckList().get(j));
						}
					}
				}
				String khapa = getEpuckList().get(i).getK();
				System.out.println("setou weight com " + i + "-" + khapa);
				RobotWindow.weight.set(i, Integer.parseInt(khapa));
			}
		}

	}

	public void updateVulnerability(ArrayList<Fiducial> fiducialList, int radiationRadius)
			throws InterruptedException, IOException {

		for (int i = 0; i < getEpuckList().size(); i++) {

			if (getEpuckList().get(i).getFiducial().isActive()) {

				for (int j = 0; j < getEpuckList().size(); j++) {

					if (getEpuckList().get(j).getFiducial().isActive()) {

						Point2D_I32 first = getEpuckList().get(i).getFiducial().getCenter();
						Point2D_I32 second = getEpuckList().get(j).getFiducial().getCenter();

						double distance = DrawFiducial.getEuclidianDistance(first, second);

						if ((distance <= radiationRadius) && (distance >= 5.0)) {

							if (!getEpuckList().get(i).getNeighbors().contains(getEpuckList().get(j))) {

								getEpuckList().get(i).addNeighbor(getEpuckList().get(j));
								System.out.println("Distance" + distance);
							}
						} else {

							if (getEpuckList().get(i).getNeighbors().contains(getEpuckList().get(j))) {

								getEpuckList().get(i).removeNeighbor(getEpuckList().get(j));
							}
						}
					} else {

						if (getEpuckList().get(i).getNeighbors().contains(getEpuckList().get(j))) {

							getEpuckList().get(i).removeNeighbor(getEpuckList().get(j));
						}
					}
				}

				try {
					String vulnerability = getEpuckList().get(i).getVulnerability();
					System.out.println("setou vulnerability de " + i + "-" + vulnerability);
					Thread.sleep(5000);

				} catch (SerialPortException e) {

					e.printStackTrace();
				}

			}
		}

	}

	public Map<Integer, String> getMapFiducial() {
		return mapFiducial;
	}

	public void setMapFiducial(Map<Integer, String> mapFiducial) {
		this.mapFiducial = mapFiducial;
	}

	public void setFiducial(Fiducial fiducial) throws InterruptedException {

		for (int i = 0; i < this.epuckList.size(); i++) {

			if (getEpuckList().get(i).getId() != null) {

				if (getEpuckList().get(i).getId().equals(this.mapFiducial.get(fiducial.getId()))) {
					getEpuckList().get(i).setFiducial(fiducial);
				}
			}
		}
	}

	public void removeFiducial(int index) throws InterruptedException {

		getEpuckList().get(index).setFiducial(null);
	}

	public void removeFiducials() {

		for (int i = 0; i < this.epuckList.size(); i++) {

			epuckList.get(i).setFiducial(null);
		}
	}

	public void setAllRobotPosition() {

		double offsetX = resolution[0].getWidth() / 2;
		double offsetY = resolution[0].getHeight() / 2;

		for (int i = 0; i < epuckList.size(); i++) {

			if (epuckList.get(i).getFiducial().isActive()) {

				epuckList.get(i).setPositionX(epuckList.get(i).getFiducial().getX());
				epuckList.get(i).setPositionY(epuckList.get(i).getFiducial().getY());

			}
		}
	}

	public void printDistancesEpuck() {

		for (int i = 0; i < getEpuckList().size(); i++) {

			if (getEpuckList().get(i).getFiducial().isActive()) {

				for (int j = 0; j < getEpuckList().size(); j++) {
					if (getEpuckList().get(j).getFiducial().isActive()) {

						Point2D_I32 first = getEpuckList().get(i).getFiducial().getCenter();
						Point2D_I32 second = getEpuckList().get(j).getFiducial().getCenter();
						double distance = DrawFiducial.getEuclidianDistance(first, second);

					}
				}
			}
		}
	}
}
